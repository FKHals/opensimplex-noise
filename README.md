OpenSimplex Noise
=================

Visually axis-decorrelated coherent noise algorithm based on the Simplectic honeycomb by Kurt Spencer.

Forked from:
https://gist.github.com/KdotJPG/b1270127455a94ac5d19